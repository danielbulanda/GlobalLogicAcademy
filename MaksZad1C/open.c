#include "open.h"
#include <stdio.h>
#include <stdlib.h>

OPEN GetOpen()
{
    OPEN open;
    FILE *fp = fopen( "test.txt", "r" );

    fgets(open.tekst2,1000,fp);
    printf("The file contains: ");
    printf("%s", open.tekst2);
    printf("\n");
    fclose( fp );                       
    return open;
}